const num = 52;
console.log(typeof num === "number");

const myName = "Milan";
const myLastName = "Prosin";
console.log(`Мене звати ${myName}, ${myLastName}`);

const myAge = 18;
console.log(`Мені ${myAge} років`);
